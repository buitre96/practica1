#ifndef ZOO_H
#define ZOO_H
#include <string>

using namespace std;

class Zoo
{

    public:
        Zoo();
        Zoo(string n);
        virtual ~Zoo();

        const int MAX = 100;

        string getNom();
        string getQuantsC();
        string getQuantsA();

        void visualitzar();
        void addAnimal(Animal* a);
        void remAnimal(Animal* a);


        bool operator<(Zoo*);
        bool operator>(Zoo*);
        bool operator==(Zoo*);

    protected:

    private:
        string nom;
        int quantsC, quantsA;

        Animal* cadells[MAX];
        Animal* adults[MAX];
};

#endif // ZOO_H
